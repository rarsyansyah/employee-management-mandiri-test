import { Injectable } from '@angular/core';
import { Employee } from '../interfaces/employee';
import { Employees } from '../../assets';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  private employees: Employee[];

  constructor() {
    this.employees = Employees as unknown as Employee[];
  }

  private save(): void {
    if (typeof localStorage !== 'undefined') {
      localStorage.setItem('employees', JSON.stringify(this.employees));
    }
  }

  getAll(): Employee[] {
    if (typeof localStorage !== 'undefined') {
      const employeesLocal = localStorage.getItem('employees');

      if (employeesLocal) {
        this.employees = JSON.parse(employeesLocal);
      }
    }

    return this.employees;
  }

  getOne(id: number): Employee {
    const idx = this.employees.findIndex((item) => item.id === id);
    return this.employees[idx];
  }

  create(props: Omit<Employee, 'id'>): void {
    this.employees.push({
      ...props,
      id: this.employees[this.employees.length - 1].id + 1,
    });

    this.save();
  }

  update(props: Employee): void {
    const idx = this.employees.findIndex((item) => item.id === props.id);
    this.employees[idx] = props;

    this.save();
  }

  delete(id: number): void {
    const idx = this.employees.findIndex((item) => item.id === id);
    this.employees.splice(idx, 1);

    this.save();
  }
}
