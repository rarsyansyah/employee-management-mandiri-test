import { Component, OnInit } from '@angular/core';
import { AuthService } from './core/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit {
  title = 'employee-management';
  isLogin: boolean = false;
  isCollapsed = false;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.isLogin = this.authService.getIsAuthenticated();
  }
}
