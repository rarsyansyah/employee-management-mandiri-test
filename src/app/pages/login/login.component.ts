import { Component } from '@angular/core';
import { AuthService } from '../../core/auth.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import {
  FormControl,
  FormGroup,
  NonNullableFormBuilder,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
})
export class LoginComponent {
  loginForm: FormGroup<{
    email: FormControl<string>;
    password: FormControl<string>;
  }>;

  constructor(
    private authService: AuthService,
    private fb: NonNullableFormBuilder,
    private notification: NzNotificationService
  ) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  onLogin() {
    if (this.loginForm.valid) {
      const { email, password } = this.loginForm.value;

      if (this.authService.login(email, password)) {
        window.location.href = '/employees';
      } else {
        this.notification.error(
          'Gagal Masuk',
          'Cek kembali email dan password',
          { nzDuration: 3000 }
        );
      }
    }
  }
}
