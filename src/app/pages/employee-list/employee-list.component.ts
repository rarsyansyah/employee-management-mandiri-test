import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../core/employee.service';
import { Employee } from '../../interfaces/employee';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrl: './employee-list.component.scss',
})
export class EmployeeListComponent implements OnInit {
  employees: Employee[] = [];
  loading = true;
  pageSize = 10;
  pageIndex = 1;
  search: string = '';
  sorting: 'asc' | 'desc' | null = null;

  selectedEmployee?: Employee;
  isVisibleDetailModal = false;

  constructor(
    private employeeService: EmployeeService,
    private notificationService: NzNotificationService
  ) {}

  ngOnInit(): void {
    this.fetchEmployees();
  }

  fetchEmployees(): void {
    this.loading = true;
    this.employees = this.employeeService.getAll();
    this.loading = false;
  }

  getEmployees(): Employee[] {
    const filtered = this.employees.filter(
      (item) =>
        item.firstName.toLowerCase().includes(this.search.toLowerCase()) ||
        item.lastName?.toLowerCase().includes(this.search.toLowerCase())
    );

    if (this.sorting === 'asc')
      return filtered.sort((a, b) => a.firstName.localeCompare(b.firstName));

    if (this.sorting === 'desc')
      return filtered.sort((a, b) => b.firstName.localeCompare(a.firstName));

    return filtered;
  }

  filteredEmployees(): Employee[] {
    return this.getEmployees().slice(
      (this.pageIndex - 1) * this.pageSize,
      this.pageIndex * this.pageSize
    );
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    const { pageIndex, pageSize } = params;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
  }

  onPageIndexChange(index: number): void {
    this.pageIndex = index;
  }

  onPageSizeChange(size: number): void {
    this.pageSize = size;
  }

  onClose(): void {
    this.isVisibleDetailModal = false;
  }

  showEditModal(id: number): void {
    this.notificationService.warning('Aksi Edit', 'Cooming soon');
  }

  showDetailModal(id: number): void {
    this.isVisibleDetailModal = true;
    const idx = this.employees.findIndex((item) => item.id === id);
    this.selectedEmployee = this.employees[idx];
  }

  delete(id: number): void {
    const idx = this.employees.findIndex((item) => item.id === id);
    const name = this.employees[idx].firstName;

    const confirmation = confirm(`Hapus data ${name} ?`);

    if (confirmation) {
      this.employeeService.delete(id);
      this.notificationService.success(
        'Terhapus',
        `Berhasil menghapus data ${name}`
      );
    }
  }

  onSearch(text: string): void {
    this.search = text;
  }

  onFilter(value: 'asc' | 'desc'): void {
    this.sorting = value;
  }
}
