import { Component } from '@angular/core';
import {
  FormControl,
  FormGroup,
  NonNullableFormBuilder,
  Validators,
} from '@angular/forms';
import id from '@angular/common/locales/id';
import { Groups } from '../../../assets';
import { EmployeeService } from '../../core/employee.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Employee } from '../../interfaces/employee';
import dayjs from 'dayjs';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrl: './employee-form.component.scss',
})
export class EmployeeFormComponent {
  groups: string[] = Groups;

  employeeForm: FormGroup<{
    username: FormControl<string>;
    firstName: FormControl<string>;
    lastName: FormControl<string>;
    email: FormControl<string>;
    birthDate: FormControl<string>;
    basicSalary: FormControl<number>;
    status: FormControl<string>;
    group: FormControl<string>;
    description: FormControl<string>;
  }>;

  idLocale = id;

  constructor(
    private fb: NonNullableFormBuilder,
    private employeeService: EmployeeService,
    private notificationService: NzNotificationService
  ) {
    this.employeeForm = this.fb.group({
      username: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', []],
      email: ['', [Validators.email, Validators.required]],
      birthDate: ['', [Validators.required]],
      basicSalary: [0, [Validators.required, Validators.min(1)]],
      status: ['', [Validators.required]],
      group: ['', [Validators.required]],
      description: ['', []],
    });
  }

  disabledDate(d: Date): boolean {
    return dayjs(d).isAfter(dayjs().subtract(1, 'd'));
  }

  onCreate(): void {
    if (this.employeeForm.valid) {
      const values = this.employeeForm.value as unknown as Omit<Employee, 'id'>;

      this.employeeService.create(values);
      this.employeeForm.reset();
      this.notificationService.success(
        'Tersimpan',
        'Data karyawan berhasil tersimpan'
      );
    }
  }
}
