import { NgModule } from '@angular/core';
import {
  BrowserModule,
  provideClientHydration,
} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import id from '@angular/common/locales/id';
import { provideNzI18n, id_ID } from 'ng-zorro-antd/i18n';
registerLocaleData(id);

import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { IconDefinition } from '@ant-design/icons-angular';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzImageModule } from 'ng-zorro-antd/image';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeeListComponent } from './pages/employee-list/employee-list.component';
import { EmployeeFormComponent } from './pages/employee-form/employee-form.component';
import { LoginComponent } from './pages/login/login.component';

import {
  MailOutline,
  KeyOutline,
  MenuFoldOutline,
  MenuUnfoldOutline,
  UserOutline,
  FileOutline,
  TeamOutline,
  LogoutOutline,
  UserAddOutline,
  DashboardOutline,
  CalendarOutline,
  DollarOutline,
  EllipsisOutline,
  FilterOutline,
  SearchOutline,
} from '@ant-design/icons-angular/icons';
import { AuthLayoutComponent } from './components/auth-layout/auth-layout.component';
import { registerLocaleData } from '@angular/common';
import { EmployeeDetailModalComponent } from './components/employee-detail-modal/employee-detail-modal.component';
import { EmployeeListMobileComponent } from './components/employee-list-mobile/employee-list-mobile.component';
import { EmployeeListTableComponent } from './components/employee-list-table/employee-list-table.component';
import { EmployeeListSearchComponent } from './components/employee-list-search/employee-list-search.component';

const icons: IconDefinition[] = [
  TeamOutline,
  UserOutline,
  FileOutline,
  MailOutline,
  KeyOutline,
  MenuFoldOutline,
  MenuUnfoldOutline,
  LogoutOutline,
  UserAddOutline,
  DashboardOutline,
  CalendarOutline,
  DollarOutline,
  EllipsisOutline,
  FilterOutline,
  SearchOutline,
];

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    EmployeeFormComponent,
    LoginComponent,
    AuthLayoutComponent,
    EmployeeDetailModalComponent,
    EmployeeListMobileComponent,
    EmployeeListTableComponent,
    EmployeeListSearchComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NzInputModule,
    NzButtonModule,
    NzIconModule.forChild(icons),
    NzNotificationModule,
    NzLayoutModule,
    NzBreadCrumbModule,
    NzTableModule,
    NzModalModule,
    NzMenuModule,
    NzListModule,
    NzPaginationModule,
    NzDatePickerModule,
    NzRadioModule,
    NzSelectModule,
    NzImageModule,
  ],
  providers: [provideClientHydration(), provideNzI18n(id_ID)],
  bootstrap: [AppComponent],
})
export class AppModule {}
