import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeListMobileComponent } from './employee-list-mobile.component';

describe('EmployeeListMobileComponent', () => {
  let component: EmployeeListMobileComponent;
  let fixture: ComponentFixture<EmployeeListMobileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmployeeListMobileComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EmployeeListMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
