import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'employee-list-search',
  templateUrl: './employee-list-search.component.html',
  styleUrl: './employee-list-search.component.scss',
})
export class EmployeeListSearchComponent {
  @Input({ required: true }) sorting!: 'asc' | 'desc' | null;
  @Input({ required: true }) search!: string;

  @Output() onSearch = new EventEmitter<string>();
  @Output() onFilter = new EventEmitter<'asc' | 'desc'>();

  handleSearch(text: string): void {
    this.onSearch.emit(text);
  }

  handleFilter(value: 'asc' | 'desc'): void {
    this.onFilter.emit(value);
  }
}
