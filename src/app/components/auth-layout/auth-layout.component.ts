import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrl: './auth-layout.component.scss',
})
export class AuthLayoutComponent {
  isCollapsed = true;
  activeRoute?: string;

  constructor(private router: Router, private authService: AuthService) {}

  onLogout(): void {
    const confirmation = confirm('Yakin ingin keluar ?');

    if (confirmation) {
      this.authService.logout();
      window.location.href = '/login';
    }
  }
}
