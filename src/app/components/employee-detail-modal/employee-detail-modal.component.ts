import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Employee } from '../../interfaces/employee';

@Component({
  selector: 'employee-detail-modal',
  templateUrl: './employee-detail-modal.component.html',
  styleUrl: './employee-detail-modal.component.scss',
})
export class EmployeeDetailModalComponent {
  @Input({ required: false }) employee?: Employee;
  @Input({ required: true }) isVisible!: boolean;
  @Output() onOk = new EventEmitter();
  @Output() onCancel = new EventEmitter();

  handleOk(): void {
    this.onOk.emit();
  }

  handleCancel(): void {
    this.onCancel.emit();
  }
}
