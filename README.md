# Sistem Manajemen Karyawan

### _Fronted Developer Test - Bank Mandiri_

_by Rizky Arsyansyah Rinjani_

###### Aplikasi yang dibutuhkan :

- Google Chrome
- NodeJS 21.6.0
- npm 10.8.1
- @angular/cli@18.0.3

###### Cara Menjalankan :

1. Clone repository
2. Masuk ke direktori project
3. Jalankan perintah `npm install` pada command line
4. Setelah semua package terdownload, jalankan `npm start`
5. Buka browser sesuai url yang tertera pada command line, defaultnya klik [link berikut](http://localhost:4200 "Link Aplikasi")

###### Akun Admin :

- Email : admin@mandiri.com
- Password : admin123
